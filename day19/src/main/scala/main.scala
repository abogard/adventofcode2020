import scala.io.Source.fromResource
import utils.{IsNum, IsWord}

object Main extends App {
  val ruleRegex = raw"\s*((?:\d+)(?: \d+)*)(?: \| ((?:\d+)(?: \d+)*))*".r
  val input = fromResource("part1.txt").getLines()
  val (rules, messages) = input.span{_.nonEmpty}

  sealed trait Rule
  case class Jump(ruleIdx: Int) extends Rule
  case class Simple(series: String) extends Rule
  case class Complex(rules: Int*) extends Rule

  val parsedRules =
    rules
      .map{_ split ':'}
      .map{case Array(IsNum(num), rule) => (num.toInt, rule.trim match {
        case IsNum(num)               => LazyList{Jump(num.toInt)}
        case s""""${IsWord(word)}"""" => LazyList{Simple(word)}
        case ruleRegex(hd, tl)        => LazyList(Option(hd), Option(tl))
          .flatten
          .map{_ split " " map {_.toInt}}
          .map{Complex(_:_*)}
      })}.toMap

  val parsedMessages = messages to Array filter {_.nonEmpty}

  def checkRules(msg: String, rules: Map[Int, LazyList[Rule]], rule: Int = 0): LazyList[String] = rules(rule) flatMap {
    case Jump(index)                          => checkRules(msg, rules, index)
    case Simple(word) if msg.startsWith(word) => LazyList{msg substring word.length}
    case Complex(indices@_*)                  => indices.foldLeft{LazyList(msg)} {
      (acc, item) => acc flatMap {checkRules(_, rules, item)}
    }
    case _ => LazyList.empty
  }

  def isValid(msg: String, rules: Map[Int, LazyList[Rule]]): Boolean =
    checkRules(msg, rules) exists {_.isBlank}

  def part1(): Unit = {
    println(s"Part 1: ${parsedMessages.count{isValid(_, parsedRules)}}")
  }

  def part2(): Unit = {
    val extendedRules =
      parsedRules
        .updated(8,  LazyList(Jump{42}, Complex(42, 8)))
        .updated(11, LazyList(Complex(42, 31), Complex(42, 11, 31)))

    println(s"Part 2: ${parsedMessages.count{isValid(_, extendedRules)}}")
  }

  part1()
  part2()
}