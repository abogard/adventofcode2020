import scala.io.Source.fromResource
import cats.implicits._
import cc.redberry.rings.ChineseRemainders.ChineseRemainders
import utils.IsNum

object Main extends App {
  val (timestamp, buses) =
    fromResource("part1.txt")
      .getLines()
      .splitAt(1)

  val (nums, remainders) =
    buses
      .flatMap{_.split(',')}
      .zipWithIndex
      .collect{case (IsNum(num),i) => (num, (num - i) % num)}
      .toArray
      .unzip

  def part1(): Unit = {
    val minTimestamp =
      timestamp
        .next
        .toLong

    val (minutes, id) = nums
      .map{busId =>
        val nextBus = minTimestamp.toFloat / busId
        (nextBus.ceil.toInt * busId - minTimestamp, busId)
      }.min

    println(s"Part 1: ${minutes * id}")
  }

  def part2(): Unit = {
    val result = ChineseRemainders(nums, remainders)

    println(s"Part 2: $result")
  }

  part1()
  part2()
}