import utils.IsNum

import scala.io.Source.fromResource

object Main extends App {
  val passports =
    fromResource("part1.txt")
      .getLines()
      .toArray

  object HasPostfix {
    def unapply(text: String): Option[(Long, String)] = {
      val (num, post) = text.span(_.isDigit)

      IsNum
        .unapply(num)
        .map(_ -> post)
    }
  }

  val groups = LazyList.unfold(passports){
    case Array() => None
    case arr =>
      val (l, r) = arr.span(_.nonEmpty)
      val fields = for {
        fields <- l
        field  <- fields.split(' ') if !field.isBlank
        split  = field.split(':') if split.size == 2
      } yield split(0) -> split(1)

      Some(fields.toMap -> r.drop(1))
  }.filter{_.nonEmpty}

  def part1() = {
    val validCount = groups map{_ - "cid"} count{_.size == 7}

    println(s"Part 1: $validCount")
  }

  def part2() = {
    val pattern = "([a-f0-9]+)".r

    val valid = groups.map{_.filter {
      case ("byr", IsNum(num)) => num >= 1920L && num <= 2002
      case ("iyr", IsNum(num)) => num >= 2010L && num <= 2020
      case ("eyr", IsNum(num)) => num >= 2020 && num <= 2030
      case ("hgt", HasPostfix(num, "cm")) => num >= 150 && num <= 193
      case ("hgt", HasPostfix(num, "in")) => num >= 59 && num <= 76
      case ("ecl", color) => Set("amb", "blu", "brn", "gry", "grn", "hzl", "oth") contains color
      case ("hcl", s"#${pattern(color)}") => color.length == 6
      case ("pid", num@IsNum(_)) => num.size == 9
      case _ => false
    }.size}

    println(s"Part 2: ${valid.count(_ == 7)}")
  }

  part1()
  part2()
}