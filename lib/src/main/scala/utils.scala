import cats.Monoid
import cats.implicits._
import shapeless.ops.hlist.Tupler
import shapeless.{HList, Lazy, Nat, Poly1}
import shapeless.ops.nat.ToInt
import shapeless.ops.sized.ToHList
import shapeless.ops.tuple.FlatMapper

import scala.collection.SortedSet
import shapeless.syntax.sized._

import scala.reflect.ClassTag

package object utils {
  implicit class FlattenSC[T](iter: LazyList[Option[T]]) {
    def flattenSC: LazyList[T] =
      iter
        .takeWhile{_.isDefined}
        .flatten
  }

  implicit class FlatMapSC[U](iter: LazyList[U]) {
    def flatMapSC[T](fun: U => Option[T]): LazyList[T] =
      iter
        .map{fun}
        .flattenSC
  }

  implicit def toPredicate[V](value: V): V => Boolean = x => x == value

  object Split {
    def unapply[U](set: SortedSet[U]): Option[(U, SortedSet[U])] =
      set.headOption.map{_ -> set.tail}
  }

  implicit class Mapper[T : Monoid](item: Option[T]) {
    def default: Option[T] = item match {
      case None  => Some(implicitly[Monoid[T]].empty)
      case other => other
    }
  }

  implicit class Multiply[T](tuple: (T, T))(implicit num: Numeric[T]) {
    def |*|(other: (T, T)): (T, T) = {
      val (x1, y1) = tuple
      val (x2, y2) = other

      (num.times(x1, x2), num.times(y1, y2))
    }

    def |*|(other: T): (T, T) = tuple |*| (other, other)
  }

  object IsNum {
    def unapply(text: String): Option[Long] = Option(text)
      .filter{_.nonEmpty}
      .collect{case num if num.forall(_.isDigit) => num.toLong}
  }

  object IsWord {
    def unapply(text: String): Option[String] = Option(text)
        .collect{case letters if letters.forall(_.isLetter) => letters}
  }

  implicit class CrossProduct[S](l: Iterable[S]) {
    def cross[T](r: Iterable[T]): Iterable[(S, T)] = for {
      a <- l
      b <- r
    } yield (a, b)
  }

  implicit class BinaryString(s: String) {
    def binaryToLong: Long = BigInt(s, 2).toLong
  }

  implicit class ToTuple[S](iter: Iterable[S]) {
    def toTuple[L0 <: HList, T](n: Nat)(implicit toInt : ToInt[n.N], hl: ToHList.Aux[List[S], n.N, L0], t: Tupler.Aux[L0, T]): T =
      iter.toList.sized(n).get.tupled
  }

  trait LowPriorityFlatten extends Poly1 {
    implicit def default[T] = at[T](Tuple1(_))
  }

  object flatten extends LowPriorityFlatten {
    implicit def caseTuple[P <: Product](implicit lfm: Lazy[FlatMapper[P, flatten.type]]) =
      at[P](lfm.value(_))
  }

  implicit class Matrix[T: ClassTag](matrix: Array[Array[T]]) {
    def rotateBy90: Array[Array[T]] = matrix.transpose.map{_.reverse}
    def rotations: Seq[Array[Array[T]]] = {
      val deg90 = matrix.rotateBy90
      val deg180 = deg90.rotateBy90
      val deg270 = deg180.rotateBy90

      Seq(matrix, deg90, deg180, deg270)
    }

    def vFlip: Array[Array[T]] = matrix.reverse
    def hFlip: Array[Array[T]] = matrix.map{_.reverse}
    def flips: Seq[Array[Array[T]]] = Seq(matrix, matrix.hFlip, matrix.vFlip, matrix.vFlip.hFlip)
  }

  implicit class Zipper[T](zipper: Iterable[Iterable[T]]) {
    def zipItself: Iterable[Iterable[T]] = LazyList.unfold{zipper}{zip =>
      zip
        .to(LazyList)
        .traverse{_.headOption}
        .map{_ -> zip.map{_.tail}}
    }
  }
}