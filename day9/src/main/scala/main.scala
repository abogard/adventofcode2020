import scala.io.Source.fromResource

object Main extends App {
  val preambleSize = 25
  val numbers = fromResource("part1.txt")
    .getLines
    .map{_.toLong}
    .toArray
    .reverse

  def findWrongNumber: Option[Long] =
    numbers
      .sliding(preambleSize + 1)
      .collectFirst{case Array(a, b@_*) if b.combinations(2).forall{_.sum != a} => a}

  def part1() {
    println(s"Part 1: $findWrongNumber")
  }

  def part2(): Unit = for {
    wrongNumber <- findWrongNumber
    consecutiveNumbers <-
      numbers
        .tails
        .flatMap {_
          .reverse
          .tails
          .find{slice => slice.size > 1 && slice.sum == wrongNumber}
        }.nextOption
  } println(s"Part 2: ${consecutiveNumbers.min + consecutiveNumbers.max}")

  part1()
  part2()
}