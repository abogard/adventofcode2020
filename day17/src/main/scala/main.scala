import scala.io.Source.fromResource
import cats.implicits._
import cats.kernel.Semigroup
import shapeless.syntax.std.tuple._
import shapeless.poly._

import LazyList.iterate
import utils.{CrossProduct, flatten}

object Main extends App {
  val directions = Seq(-1, 0, 1)

  def createMap[T](fun: (Int, Int) => T): Iterator[T] = for {
    (line, y) <- fromResource("part1.txt")
      .getLines
      .zipWithIndex
    (char, x) <- line.zipWithIndex if char == '#'
  } yield fun(x, y)

  def play[T: Semigroup](map: Set[T], dirs: Set[T]): LazyList[Set[T]] =
    iterate{map}{acc => acc
      .flatMap{p => dirs.map{p |+| _} + p}
      .foldLeft{acc}{case (m, p) => (acc contains p, dirs map {p |+| _} count {acc contains _}) match {
          case (false, 3) => m + p
          case (true, count) if 2 to 3 contains count => m
          case _ => m - p
        }
      }
    }

  def part1(): Unit = {
    val directions3d = (directions cross directions cross directions)
      .map{tuple => flatten(tuple)}
      .filter{_ != (0, 0, 0)}
      .toSet

    val game = play(createMap{(x, y) => (x, y, 0)}.toSet, directions3d)

    println(s"Part 1: ${game.take{7}.last.size}")
  }

  def part2(): Unit = {
    val directions4d = (directions cross directions cross directions cross directions)
      .map{tuple => flatten(tuple)}
      .filter{_ != (0, 0, 0, 0)}
      .toSet

    val game = play(createMap{(x, y) => (x, y, 0, 0)}.toSet, directions4d)

    println(s"Part 2: ${game.take(7).last.size}")
  }

  part1()
  part2()
}