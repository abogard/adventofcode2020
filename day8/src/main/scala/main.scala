import scala.io.Source.fromResource

import cats.implicits._

object Main extends App {
  val accOp = "acc"
  val jmpOp = "jmp"
  val nopOp = "nop"
  val instructions = fromResource("part1.txt")
    .getLines()
    .map(_.split(' '))
    .collect{case Array(op, num) => (num.toLong, op)}
    .toArray
  val instructionsNum = instructions.size

  type Output[T] = Either[Long, T]

  def runComputer(replaceInst: Long = -1L): Either[Long, Long] = (0L, 0L, Set.empty[Long]).tailRecM[Output, Long]{
    case (op, acc, checked) if checked.contains(op) => Left(acc)
    case (`instructionsNum`, acc, _) => Right(Right(acc))
    case (op, acc, checked) => Right {
      val newChecked = checked + op

      instructions(op.toInt).map{_ -> op} match {
        case (_  , (`nopOp`, _) | (`jmpOp`, `replaceInst`)) => Left(op + 1, acc, newChecked)
        case (arg, (`jmpOp`, _) | (`nopOp`, `replaceInst`)) => Left(op + arg, acc, newChecked)
        case (arg, (`accOp`, _)) =>
          val newAcc = acc + arg

          Left(op + 1, newAcc, newChecked)
      }
    }
  }

  def part1() {
    val result = runComputer()

    println(s"Part 1: $result")
  }

  def part2(): Unit = {
    val result =
      instructions
        .zipWithIndex
        .collect{case ((_, `jmpOp` | `nopOp`), i) => runComputer(i)}
        .flatMap{_.toOption}
        .headOption

    println(s"Part 2: $result")
  }

  part1()
  part2()
}