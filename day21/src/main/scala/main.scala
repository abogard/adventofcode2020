import scala.io.Source.fromResource
import scala.LazyList.unfold

object Main extends App {
  val lineRegex = """^((?:\p{L}+ ?)+)(?:\(contains (.+)\)$)?""".r
  val recipes = {
    for {
      lineRegex(i, a) <- fromResource("part1.txt").getLines() if a != null
      is = i.split(' ').toSet
      as = a.split(',').map {_.trim}
    } yield is -> as
  }.toArray

  val flatRecipes = for {
    (ingredients, allergens) <- recipes
     allergen <- allergens
  } yield allergen -> ingredients

  val allIngredients =
    recipes
      .flatMap{case (ingredients, _) => ingredients}
      .toSet

  val allergicIngredients =
    flatRecipes
      .groupMapReduce{case (al, _) => al}{case (_, in) => in}{_ intersect _}

  def part1(): Unit = {
    val nonAllergicIngredients = allIngredients -- allergicIngredients
      .values
      .toSet
      .flatten

    val nonAllergicIngredientsCount = recipes
      .map{case (ingredient, _) => nonAllergicIngredients intersect ingredient}
      .map{_.size}
      .sum

    println(s"Part 1: $nonAllergicIngredientsCount")
  }

  def part2(): Unit = {
    val mapping = unfold{allergicIngredients.toList}{
      case Nil => None
      case ingredients@_::_ =>
        val (hd@(_, in))::tl = ingredients.sortBy{case (_, als) => als.size}

        Some(hd, tl map {case (als, ins) => als -> ins.diff(in)})
    }.map{case (al, in) => al -> in.head}
     .toList

    println(s"Part 2: ${mapping.sortBy{_._1}.map{_._2}.mkString(",")}")
  }

  part1()
  part2()
}