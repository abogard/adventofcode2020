import utils.IsNum

import scala.io.Source.fromResource
import utils.{BinaryString, ToTuple}

object Main extends App {
  val bits = Set('1', '0')
  val maskRegex = raw"mask = (.+)".r
  val memRegex = raw"mem\[(\d+)\] = (\d+)".r

  def lines: Iterator[String] = fromResource("part1.txt").getLines

  def createCombinations(series: Iterable[(Char,Char)]): LazyList[String] =  series.headOption match {
    case Some(l -> r) =>
      val rest = createCombinations(series.tail)
      val mapping = l match {
        case 'X' => bits.to(LazyList)
        case '1' => LazyList('1')
        case '0' => LazyList(r)
      }

      mapping.flatMap{bit => rest.map{bit + _}}
    case None => LazyList("")
  }

  def part1(): Unit = {
    val (_, result) =
      lines
        .foldLeft{(1L,0L) -> Map[Long, Long]()}{
          case ((mask@(and, or), acc), memRegex(IsNum(to), IsNum(input))) =>
            mask -> acc.updated(to, input & and | or)
          case ((_, acc), maskRegex(mask)) =>
            bits
              .map{mask.replace('X', _).binaryToLong}
              .toTuple(2) -> acc
        }

    println(s"Part 1: ${result.values.sum}")
  }

  def part2(): Unit = {
    val (_, result) =
      lines
        .foldLeft{"" -> Map[Long,Long]()}{
          case ((_, reg), maskRegex(mask)) => mask -> reg
          case ((mask, reg), memRegex(IsNum(to), IsNum(input))) =>
            val series = mask
              .reverse
              .zipAll(to.toBinaryString.reverse, '0', '0')
            val newReg = createCombinations(series)
              .foldLeft{reg}{(reg, addr) => reg.updated(addr.binaryToLong, input)}

            mask -> newReg
         }

    println(s"Part 2: ${result.values.sum}")
  }

  part1()
  part2()
}
