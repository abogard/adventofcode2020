import scala.io.Source.fromResource
import scala.LazyList.iterate
import scala.collection.mutable.HashMap

object Main extends App {
  val nums =
    fromResource("part1.txt")
      .getLines()
      .flatMap{_.split(',')}
      .zipWithIndex
      .map{case (num, idx) => (num.toInt, idx + 1)}
      .toArray

  val numsMap =
    HashMap
      .from(nums)
      .withDefaultValue(-1)

  val spokenNumbers = iterate{(numsMap, nums.last._1 -> -1, nums.size)}{
    case (acc, (n, pr), cr) =>
      val updated = acc.addOne(n, cr)
      val num = if (pr == - 1) 0 else cr  - pr

      (updated, num -> updated(num), cr + 1)
  }.map{case (_, spoken, _) => spoken}
   .prependedAll(nums.dropRight(1))
   .map{case (num, _) => num}

  def part1(): Unit = {
    println(s"Part 1: ${spokenNumbers.take(2020).last}" )
  }

  def part2(): Unit = {
    println(s"Part 2: ${spokenNumbers.take(30000000).last}" )
  }

  part1()
  part2()
}