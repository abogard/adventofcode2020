import scala.io.Source.fromResource

object Main extends App {
  val END = "shiny gold"

  val pattern = raw"(.+) bags contain (\d+ .+ bag.?,?)+.".r
  val split = raw"\s*(\d+) (.+) bags?".r
  val graph = fromResource("part1.txt")
    .getLines()
    .collect {
      case pattern(current, connected) =>
        current.strip() -> connected
          .split(',')
          .collect{case split(count, tpe) => (tpe.strip(), count.toLong)}
          .toMap
     }.toMap
      .withDefaultValue(Map.empty)

  def findBag(current: String): Boolean =
    current == END ||
      graph(current)
        .keys
        .exists{findBag}

  def countBags(current: String): Long = 1L +
    graph(current)
      .map{case (n, c) => c * countBags(n)}
      .sum

  def part1() {
    val result = graph.keySet - END filter findBag

    println(s"Part 1: ${result.size}")
  }

  def part2() {
    println(s"Part 2: ${countBags(END)}")
  }

  part1()
  part2()
}