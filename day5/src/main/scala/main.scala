import scala.io.Source.fromResource

object Main extends App {
  val MAX_ROWS = 127
  val MAX_COLS = 7

  val seats = fromResource("part1.txt")
    .getLines
    .toArray

  def toLeft(left: Int, right: Int): (Int, Int) =
    (left, left + (right - left) / 2)

  def toRight(left: Int, right: Int): (Int, Int) =
    (left + (right - left) / 2 + 1, right)

  def calculateId(row: Int, col: Int): Int = row * 8 + col

  val rows = seats.map{_.foldLeft{(0, MAX_ROWS)}{
    case (acc, 'F') => (toLeft (_, _)).tupled(acc)
    case (acc, 'B') => (toRight (_, _)).tupled(acc)
    case (acc, _) => acc
  }}.map{_._1}

  val cols = seats.map{_.foldLeft{(0, MAX_COLS)}{
    case (acc, 'L') => (toLeft (_, _)).tupled(acc)
    case (acc, 'R') => (toRight (_, _)).tupled(acc)
    case (acc, _) => acc
  }}.map{_._1}

  def part1(): Unit = {
    val result = rows zip cols map{(calculateId(_, _)).tupled}

    println(s"Part 1: ${result.max}")
  }

  def part2(): Unit = {
    val existing = rows zip cols map{(calculateId(_, _)).tupled}
    val all = for {
      row <- 0 to MAX_ROWS
      col <- 0 to MAX_COLS
    } yield calculateId(row, col)
    val diff = all.toSet -- existing

    val result = diff.filter{id => existing.exists(_ == id + 1) && existing.exists(_ == id -1)}

    println(s"Part 2: ${result.headOption}")
  }

  part1()
  part2()
}