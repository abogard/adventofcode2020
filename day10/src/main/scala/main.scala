import scala.io.Source.fromResource

import cats.implicits._
import LazyList.{from, unfold}
import scala.collection.SortedSet
import utils.{Split, Mapper}

object Main extends App {
  val connectors = fromResource("part1.txt")
    .getLines
    .map{_.toInt}
    .toArray

  val min = 0
  val max = connectors.max + 3
  val extended_connectors = connectors
    .appended(min)
    .appended(max)

  val asGraph =
    extended_connectors
      .map{case num => num ->
        from(num + 1)
          .take(3)
          .filter(extended_connectors contains _)
      }.toMap

  def part1(): Unit = {
    val result = extended_connectors
      .sortBy{-_}
      .sliding(2)
      .map{case Array(g, l) => Map(g - l -> 1)}
      .reduce{_ |+| _}

    println(s"Part 1: ${result(1) * result(3)}")
  }

  type Id = Int
  type Depth = Int
  type Node = (Depth, Id)

  def part2(): Unit = {
    val start  = (0, min): Node
    val result = unfold(Map(start -> 1L), SortedSet(start)) {
      case (paths, Split(node@(depth, id), rest)) =>
        val count    = paths(node)
        val children = asGraph(id) map {depth + 1 -> _}
        val newPaths = children.foldLeft{paths}{_.updatedWith(_)(_.default map {_ + count})}
        val newQueue = rest ++ (children.toSet -- paths.keySet)

        Some{newPaths -> (newPaths, newQueue)}
      case _ => None
    }.last

    println(s"Part 2: ${result.collect{case (_, `max`) -> v => v}.sum}")
  }

  part1()
  part2()
}