import scala.io.Source.fromResource
import utils.IsNum
import LazyList.unfold

object Main extends App {
  val rangeRegex = raw"(\d+)-(\d+)"
  val rulesRegex = s"(.+): (?:(?:$rangeRegex)(?: or $rangeRegex)*)".r
  val Array(rules, tickets@_*) =
    fromResource("part1.txt")
      .getLines()
      .mkString("\n")
      .split("\n\n")

  val parsedRules =
    rules
      .split('\n')
      .collect{case rulesRegex(field, rules@_*) => field ->
        rules
          .map{_.toInt}
          .grouped(2)
          .map{case Seq(a, b) => a to b}
          .toSet
      }.toMap

  val Seq(ourParsedTicket, otherParsedTicket) =
    tickets
      .map{_ split '\n' map {_ split ','} map {_ collect {case IsNum(n) => n.toInt}}}
      .map{_ filter {_.nonEmpty}}

  val invalidTickets =
    otherParsedTicket
      .zipWithIndex
      .map{case (ticket, idx) => idx -> ticket
        .filter{num =>
          parsedRules
            .values
            .forall{!_.exists{_ contains num}}
        }
      }.filter{case (_, inv) => inv.nonEmpty}
       .toMap

  val validTickets =
    otherParsedTicket
      .zipWithIndex
      .filterNot{case (_, idx) => invalidTickets contains idx}
      .map{case (rules, _) => rules} ++ ourParsedTicket

  def part1(): Unit = {
    val result = invalidTickets
      .values
      .map{_.sum}
      .sum

    println(s"Part 1: $result")
  }

  def part2(): Unit = {
    val validRules = validTickets.map{_
      .map{field =>
        parsedRules
          .collect{case (k, r) if r.exists(_ contains field) => k}
          .toSet
      }
    }.transpose
     .map{_.reduce(_ intersect _)}

    val labels = unfold{validRules.zipWithIndex.toSeq}{
      case Seq() => None
      case acc@Seq(_, _@_*) =>
        val Seq(hd@(label, _), tl@_*) = acc.sortBy{case(labels, _) => labels.size}

        Some(hd -> tl.map{case (labels, i) => (labels diff label, i)})
    }.collect{case (s, i) => s.head -> i}
     .toMap

    val result = labels
      .view
      .filterKeys{_ contains "departure"}
      .mapValues{ourParsedTicket(0)(_).toLong}
      .values
      .reduce{_ * _}

    println(s"Part 2: $result")
  }

  part1()
  part2()
}