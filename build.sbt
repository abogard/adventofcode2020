name := "AdventOfCode2020"

version := "0.1"

lazy val common = Seq(
  scalaVersion := "2.13.4",
  libraryDependencies := Seq(
    "org.typelevel" %% "cats-core" % "2.3.0",
    "cc.redberry" % "rings" % "2.5.7",
    "com.chuusai" %% "shapeless" % "2.3.3"
  )
)

lazy val lib = project in file("lib") settings common
lazy val day1 = project in file("day1") settings common
lazy val day2 = project in file("day2") settings common
lazy val day3 = project in file("day3") settings common
lazy val day4 = project in file("day4") settings common dependsOn lib
lazy val day5 = project in file("day5") settings common
lazy val day6 = project in file("day6") settings common
lazy val day7 = project in file("day7") settings common
lazy val day8 = project in file("day8") settings common
lazy val day9 = project in file("day9") settings common
lazy val day10 = project in file("day10") settings common dependsOn lib
lazy val day11 = project in file("day11") settings common dependsOn lib
lazy val day12 = project in file("day12") settings common dependsOn lib
lazy val day13 = project in file("day13") settings common dependsOn lib
lazy val day14 = project in file("day14") settings common dependsOn lib
lazy val day15 = project in file("day15") settings common dependsOn lib
lazy val day16 = project in file("day16") settings common dependsOn lib
lazy val day17 = project in file("day17") settings common dependsOn lib
lazy val day18 = project in file("day18") settings common dependsOn lib
lazy val day19 = project in file("day19") settings common dependsOn lib
lazy val day20 = project in file("day20") settings common dependsOn lib
lazy val day21 = project in file("day21") settings common dependsOn lib
lazy val day22 = project in file("day22") settings common dependsOn lib
lazy val day23 = project in file("day23") settings common
lazy val day24 = project in file("day24") settings common dependsOn lib
lazy val day25 = project in file("day25") settings common dependsOn lib

lazy val root = project in file(".") aggregate(lib, day1, day2, day3, day4, day5, day6, day7, day8, day9, day10, day11,
  day12, day13, day14, day15, day16, day17, day18, day19, day20, day21, day22, day23, day24, day25)