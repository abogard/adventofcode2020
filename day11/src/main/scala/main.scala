import scala.io.Source.fromResource
import LazyList.iterate
import utils.{FlatMapSC, toPredicate}
import cats.implicits._

object Main extends App {
  type Point = (Int, Int)
  type SeatsMap = Map[Point, Char]

  val zero = (0, 0)
  val directions =
    Seq((-1, 0), (1, 0), (0, -1), (0, 1), zero)
      .combinations(2)
      .map{_.reduce(_ |+| _)}
      .filterNot{_ == zero}
      .toArray

  val seatsMap = {
    for {
      (line, y) <- fromResource("part1.txt").getLines.zipWithIndex
      (char, x) <- line.zipWithIndex
    } yield (x -> y, char)
  }.toMap

  def calculateSteps(map: SeatsMap, stepSize: Int = 1, occupiedLimit: Int = 4): SeatsMap = map.foldLeft{map} {
    case (acc, (_, '.')) => acc
    case (acc, (pos, char)) =>
      val neighbours = directions count {direction =>
        iterate{pos}{_ |+| direction}
          .tail
          .take(stepSize)
          .flatMapSC{map get _ filterNot 'L'}
          .exists{'#'}
      }

      char match {
        case '#' if neighbours >= occupiedLimit => acc.updated(pos, 'L')
        case 'L' if neighbours == 0 => acc.updated(pos, '#')
        case _ => acc
      }
  }

  def countOccupiedSeats(stepSize: Int = 1, occupiedLimit: Int = 4): Int =
    iterate{seatsMap}{calculateSteps(_, stepSize, occupiedLimit)}
      .tapEach()
      .sliding(2)
      .collectFirst{case Seq(l, r) if l == r => r.values}
      .fold{0}{_.count('#')}

  def part1(): Unit = {
    println(s"Part 1: ${countOccupiedSeats()}")
  }

  def part2(): Unit = {
    println(s"Part 2: ${countOccupiedSeats(Int.MaxValue, 5)}")
  }

  part1()
  part2()
}