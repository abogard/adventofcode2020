import cats.Id
import cats.implicits._

import scala.io.Source.fromResource
import utils.IsNum

object Main extends App {
  def tokens =
    fromResource("part1.txt")
      .getLines()
      .map{_ split " |(|)" filterNot {_.isEmpty}}
      .map{_.toList}

  sealed trait Expression
  case class Brackets(expr: Expression) extends Expression
  case class Num(number: Long) extends Expression
  case class Op(opcode: Char, left: Expression, right: Expression) extends Expression

  type State = (List[String], Expression)

  def parse(stack: List[String]): State = (stack, null: Expression).tailRecM[Id, State]{
    case (Nil, expr)                 => Right(Nil, expr)
    case (")"::tl, expr)             => Right(tl, expr)
    case ("("::tl, _)                => Left(parse(tl) map {Brackets(_)})
    case (IsNum(num)::tl, _)         => Left(tl, Num(num))
    case ((op@("*"|"+"))::tl, lExpr) => Left{(
      tl match {
        case "("::tl        => parse(tl) map {Brackets(_)}
        case IsNum(num)::tl => (tl, Num(num))
      }) map {Op(op(0), lExpr, _)}
    }
    case _ => ???
  }

  def part1(): Unit = {
    def eval(expr: Expression): Long = expr match {
      case Num(n)        => n
      case Brackets(e)   => eval{e}
      case Op('+', l, r) => eval{l} + eval{r}
      case Op('*', l, r) => eval{l} * eval{r}
    }

    println(s"Part 1: ${tokens.map{parse}.map{case (_, e) => eval(e)}.sum}")
  }

  def part2(): Unit = {
    def eval(expr: Expression): Long = expr match {
      case Num(n)      => n
      case Brackets(e) => eval{e}
      case Op('+', op@Op(_  , _, rn), r) => eval{op.copy(right = Num(eval{r} + eval{rn}))}
      case Op('+', l, r) => eval{l} + eval{r}
      case Op('*', l, r) => eval{l} * eval{r}
    }

    println(s"Part 2: ${tokens.map{parse}.map{case (_, e) => eval(e)}.sum}")
  }

  part1()
  part2()
}