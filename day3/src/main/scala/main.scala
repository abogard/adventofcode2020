import scala.io.Source.fromResource

object Main extends App {
  def map: Iterator[String] = fromResource("part1.txt").getLines()

  def filterChars(cols: Int, rows: Int = 1): Iterator[Char]  = for {
    (line, row) <- map.zipWithIndex if row % rows == 0
    col =  (row / rows * cols) % line.length
  } yield line(col)

  def countTrees(cols: Int, rows: Int = 1): Int =
    filterChars(cols, rows)
      .filter{_ == '#'}
      .size

  def part1(): Unit = {
    println(s"Part 1: ${countTrees(3, 1)}")
  }

  def part2(): Unit = {
    val counts = Seq(1 -> 1, 3 -> 1, 5 -> 1, 7 -> 1, 1 -> 2)
      .map{(countTrees (_, _)).tupled}
      .map{_.toLong}
      .reduce{_ * _}

    println(s"Part 2: ${counts}")
  }

  part1()
  part2()
}