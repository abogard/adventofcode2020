import scala.io.Source.fromResource

object Main extends App {
  val pattern = raw"(\d+)-(\d+) (.): (.+)".r
  val passwords = fromResource("part1.txt")
    .getLines()
    .collect{case pattern(min, max, char, text) => (min.toInt, max.toInt, char(0), text)}
    .toArray

  def part1(): Unit = {
    val result = for {
      (min, max, char, text) <- passwords
      count = text.count(_ == char) if (min to max) contains count
    } yield text

    println(s"Part 1: ${result.size}")
  }

  def part2(): Unit = {
    val result = for {
      (min, max, char, text) <- passwords
      indices = Seq(min, max) map {_ - 1}
      chars = indices map{text(_) == char}
        if chars.reduce{_ ^ _}
    } yield text

    println(s"Part 2: ${result.size}")
  }

  part1()
  part2()
}