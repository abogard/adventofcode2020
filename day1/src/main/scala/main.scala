import scala.io.Source.fromResource

object Main extends App {
  val expected = 2020
  val numbers = fromResource("part1.txt")
    .getLines()
    .map{_.toInt}
    .toArray

  def part1(): Unit = {
    val result = numbers
      .combinations(2)
      .collectFirst{case nums if nums.sum == expected => nums.reduce(_ * _)}

    println(s"Part 1: $result")
  }

  def part2(): Unit = {
    val result = numbers
      .combinations(3)
      .collectFirst{case nums if nums.sum == expected => nums.reduce(_ * _)}

    println(s"Part 2: $result")
  }

  part1()
  part2()

}