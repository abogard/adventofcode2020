import scala.io.Source.fromResource
import utils.IsNum
import scala.collection.mutable.Queue
import scala.collection.mutable.Queue.from
import scala.collection.mutable.HashSet

object Main extends App {
  type Deck = Queue[Int]

  val (deck1, deck2) =
    fromResource("part1.txt")
      .getLines
      .span{_.nonEmpty}

  val List(parsedDeck1, parsedDeck2) = List(deck1, deck2)
    .map{_.collect{case IsNum(num) => num.toInt}}
    .map{from}

  def calculateScore(deck: Deck): Int =
    deck
      .reverseIterator
      .zipWithIndex
      .map{case (card, i) => card * (i + 1)}
      .sum

  def part1(): Unit = {
    val deck1 = parsedDeck1.clone()
    val deck2 = parsedDeck2.clone()

    while(!deck1.isEmpty && !deck2.isEmpty) {
      val card1 = deck1.dequeue
      val card2 = deck2.dequeue

      if (card1 > card2) {
        deck1.enqueue(card1, card2)
      } else {
        deck2.enqueue(card2, card1)
      }
    }

    println(s"Part 1: ${calculateScore(deck1 ++ deck2)}")
  }

  def play(deck1: Deck, deck2: Deck): Boolean = {
    val hasLoop = HashSet.empty[(Deck, Deck)]

    while (!deck1.isEmpty && !deck2.isEmpty) {

      if (!hasLoop.add{deck1.clone -> deck2.clone})
        return true

      val card1 = deck1.dequeue
      val card2 = deck2.dequeue

      val shouldRecurse = card1 <= deck1.length && card2 <= deck2.length
      val firstPlayerWon =
        if (shouldRecurse)
          play(deck1 slice (0, card1), deck2 slice (0, card2))
        else
          card1 > card2

      if (firstPlayerWon) {
        deck1.enqueue(card1, card2)
      } else {
        deck2.enqueue(card2, card1)
      }
    }

    deck1.size > deck2.size
  }

  def part2(): Unit = {
    val deck1 = parsedDeck1.clone()
    val deck2 = parsedDeck2.clone()

    play(deck1, deck2)

    println(s"Part 2: ${calculateScore(deck1 ++ deck2)}")
  }

  part1()
  part2()
}