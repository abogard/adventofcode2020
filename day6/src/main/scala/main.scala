import scala.io.Source.fromResource

object Main extends App {
  val answers = fromResource("part1.txt")
    .mkString
    .split("\n\n")
    .map{_.split("\n").toSet}

  def part1(): Unit = {
    val counts = for {
      group <- answers
      anyYesAnswered = group.reduce(_ ++ _)
    } yield anyYesAnswered.size

    println(s"Part 1: ${counts.sum}")
  }

  def part2(): Unit = {
    val counts = for {
      group <- answers
      allYesAnswered = group.reduce(_ intersect _)
    } yield allYesAnswered.size

    println(s"Part 2: ${counts.sum}")
  }

  part1()
  part2()
}