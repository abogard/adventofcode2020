import cats.implicits._
import LazyList.{unfold, iterate}
import scala.io.Source.fromResource

object Main extends App {
  val mapping = Map(
    "e"  -> ( 2, 0),
    "w"  -> (-2, 0),
    "nw" -> (-1,-1),
    "sw" -> (-1, 1),
    "ne" -> ( 1,-1),
    "se" -> ( 1, 1)
  )

  val pattern = raw"(e|w|nw|sw|ne|se)(.*)".r
  val switches = fromResource("part1.txt")
    .getLines
    .map{unfold(_){
        case s if s.isEmpty     => None
        case pattern(dir, rest) => Some(dir -> rest)
      } map mapping reduce(_ |+| _)
    }.toSeq

  val tiles = switches
    .groupBy{identity}
    .filter{case (_, flips) => flips.size % 2 != 0}
    .keySet

  def part1(): Unit = {
    println(s"Part 1: ${tiles.size}")
  }

  def part2(): Unit = {
    val mappings = mapping
      .values
      .toSet

    val days = iterate(tiles){floor =>
      floor
        .flatMap{tile => mappings.map{_ |+| tile} + tile}
        .map{tile => (
          tile,
          floor contains tile,
          mappings.map{_ |+| tile}.intersect(floor).size
        )}.foldLeft(floor){
          case (acc, (tile, true,  0))          => acc - tile
          case (acc, (tile, true,  c)) if c > 2 => acc - tile
          case (acc, (tile, false, 2))          => acc + tile
          case (acc, _)                         => acc
        }
    }

    println(s"Part 2: ${days.drop(100).head.size}")
  }

  part1()
  part2()
}