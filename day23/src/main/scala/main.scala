import LazyList.iterate
import scala.io.Source.fromResource

object Main extends App {
  val labels  = fromResource("part1.txt").getLines.mkString
  val prefix  = labels map {_.toString.toLong} to LazyList
  val maximum = prefix.max
  val minimum = prefix.min

  object Node {
    def from(iter: Iterable[Long]): Node = {
      val prefix = Node(0, null, null)

      iter.foldLeft{prefix}{(l, r) => l.append(Node(r, null, null)).next}

      prefix.next
    }
  }

  case class Node(value: Long, var previous: Node, var next: Node) extends Iterable[Node] {
    override def iterator: Iterator[Node] = iterate(this){_.next} takeWhile{_ != null} to Iterator
    override protected def fromSpecific(coll: IterableOnce[Node]): Iterable[Node] = LazyList.from(coll)

    def append(other: Node): Node = {
      this.next = other
      other.previous = this

      this
    }
  }

  def play(rounds: Int, numberOfCups: Int): LazyList[Node] = {
    val cups   = prefix appendedAll iterate(maximum + 1){_ + 1} take numberOfCups
    val maxCup = cups.max
    var list   = Node.from(cups)
    val last   = list.last
    val nodes  = list map {node => node.value -> node} to Map

    last append list

    for (_ <- 1 to rounds) {
      val Array(a, b, c, d) = list drop 1 take 4 to Array

      list append d

      var destination = list.value

      do {
        destination -= 1
        destination = if (destination < minimum) maxCup else destination
      } while (destination == a.value || destination == b.value || destination == c.value)

      val node = nodes(destination)
      val next = node.next

      node append a
      c append next

      list = list.next
    }

    nodes(minimum) take numberOfCups to LazyList
  }

  def part1(): Unit = {
    val result = play(100, prefix.size)

    println(s"Part 1: ${result drop 1 map {_.value} mkString ""}")
  }

  def part2(): Unit = {
    val cups = play(10000000,1000000)
    val result = cups drop 1 take 2 map {_.value} reduce(_ * _)

    println(s"Part 2: $result")
  }

  part1()
  part2()
}