import scala.io.Source.fromResource

import utils.Multiply
import cats.implicits._

object Main extends App {
  type Point = (Long, Long)

  val mapping = Map(
    'N' -> (0L, -1L),
    'S' -> (0L,  1L),
    'E' -> (1L, 0L),
    'W' -> (-1L, 0L)
  )

  object Rotate {
    def unapply(op: String): Option[Point => Point] = op.toArray match {
      case Array(dir@('L'|'R'), rest@_*) if rest forall {_.isDigit} => Some {
        val (x, y) = if (dir == 'R') (0L, 1L) else (0L, -1L)

        point => (0 until rest.mkString.toInt / 90)
          .foldLeft {point} { case ((cx, cy), _) => (cx * x - cy * y, cx * y + cy * x) }
      }
      case _ => None
    }
  }

  object Shift {
    def unapply(op: String): Option[Point => Point] = op.toArray match {
      case Array(first, rest@_*) if rest forall {_.isDigit} => mapping get first map {dir =>
        point => point |+| (dir |*| rest.mkString.toLong)
      }
      case _ => None
    }
  }

  object Forward {
    def unapply(op: String): Option[Point => Point] = op match {
      case s"F$rest" if rest forall {_.isDigit} => Some { point => point |*| rest.toLong}
      case _ => None
    }
  }

  val instructions = fromResource("part1.txt")
    .getLines()
    .toArray

  def part1(): Unit = {
    val (_, (x, y)) = instructions.foldLeft(mapping('E'), (0L, 0L)){
      case ((dir, acc), Shift(op)) => (dir, op(acc))
      case ((dir, acc), Rotate(op)) => (op(dir), acc)
      case ((dir, acc), Forward(op)) => (dir, acc |+| op(dir))
    }

    println(s"Part 1: ${x.abs + y.abs}")
  }

  def part2(): Unit = {
    val ((x, y), _) = instructions.foldLeft((0L, 0L), (10L, -1L)){
      case ((acc, way), Shift(op)) => (acc, op(way))
      case ((acc, way), Rotate(op)) => (acc, op(way))
      case ((acc, way), Forward(op)) => (acc |+| op(way), way)
    }

    println(s"Part 2: ${x.abs + y.abs}")
  }

  part1()
  part2()
}