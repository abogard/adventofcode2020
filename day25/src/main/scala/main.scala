import scala.io.Source.fromResource
import LazyList.iterate

object Main extends App {
  val mod = 20201227
  val Array(cardsPubKey, doorsPubKey) =
    fromResource("part1.txt")
      .getLines()
      .map{_.toLong}
      .toArray

  def applyLoopSize(num: Long): LazyList[Long] =
    iterate{num}{_ * num % mod}

  def findLoopSize(num: Long): Option[Long] =
    applyLoopSize(7)
      .zipWithIndex
      .collectFirst{case (`num`, pow) => pow + 1}


  def part1(): Unit = {
    val doorsLoop = findLoopSize(doorsPubKey)
    val encryptionKey = doorsLoop flatMap{loop =>
      applyLoopSize(cardsPubKey)
        .take(loop.toInt)
        .lastOption
    }

    println(s"Part 1: $encryptionKey")
  }

  part1()
}