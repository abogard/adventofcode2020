import scala.io.Source.fromResource
import utils.{IsNum, Matrix, Zipper}
import cats.implicits._
import Array.fill

object Main extends App {
  type TileId = Long
  type Tile = List[List[Char]]
  type Point = (Int, Int)
  type Board = Map[Point, (TileId, Tile)]
  type Transformations = Array[Tile]

  val monster =
    fromResource("monster.txt")
      .getLines()
      .map{_.toCharArray}
      .toArray

  val monsterSize = monster.flatten.count{_ == '#'}
  val monsterRows = monster.length
  val monsterCols = monster.map{_.length}.max

  val tiles =
    fromResource("part1.txt")
      .mkString
      .split("\n\n")
      .map{_.split(':')}
      .collect{case Array(s"Tile ${IsNum(id)}", data) => id -> data}
      .map{case (id, data) => (id, data split '\n' filter {_.nonEmpty} map {_.toCharArray})}

  val flatTransformations = for {
    (id, tile) <- tiles
    rotation   <- tile.rotations
    flip       <- rotation.flips
  } yield id -> flip

  val transformations =
    flatTransformations
      .groupMap{case (id, _) => id}{case (_, tiles) => tiles map{_ to List} to List}
      .map{case (id, tiles) => (id, tiles to Array distinctBy identity)}

  val sideSize: Int = math
    .sqrt(transformations.size)
    .toInt

  def matchLeft(l: Tile, r: Tile): Boolean =
    l.indices map {i => l(i).last == r(i).head} reduce{_ && _}

  def matchTop(l: Tile, r: Tile): Boolean =
    l.indices map {i => l.last(i) == r.head(i)} reduce{_ && _}

  def matchMonster(l: Iterable[Array[Char]]): Boolean = {
    monster zip l forall{case (sl, sr) => sl zip sr forall {case(cl, cr) => cl == ' ' || cl == cr}}
  }

  val (possiblesBottom, possiblesRight) = { for {
      (id, tiles)   <- transformations to Iterable
      tile          <- tiles
      reduced        = transformations - id
      identifier     = (id, tile)
      remaining      = reduced to Iterable flatMap{case (nId, tiles) => tiles map {nId -> _}}
      possibleBottom = remaining collect{case (id, nTile) if matchTop(tile, nTile) => id -> nTile} to Set
      possibleRight  = remaining collect{case (id, nTile) if matchLeft(tile, nTile) => id -> nTile} to Set
    } yield (identifier -> possibleBottom, identifier -> possibleRight)
  }.unzip
   .bimap(_.toMap , _.toMap)

  def findTilesPositions(
    visitable: Set[(TileId, Tile)] = possiblesRight.keySet,
    row: Int = 0,
    col: Int = 0,
    board: Board = Map.empty
  ): LazyList[Board] = (row, col) match {
    case (`sideSize`, 0) => LazyList(board)
    case (_, _)          => for {
      identifier   <- visitable to LazyList
      newPositions  = board updated (row -> col, identifier)
      nRow          = row + (col + 1) / sideSize
      nCol          = (col + 1) % sideSize
      visitable     = (nRow, nCol) match {
        case (0, _) => possiblesRight(identifier)
        case (r, 0) => possiblesBottom(newPositions(r - 1, 0))
        case (r, c) => possiblesRight(identifier) intersect possiblesBottom(newPositions(r - 1, c))
      }
      next <- findTilesPositions(visitable, nRow, nCol, newPositions)
    } yield next
  }

  def part1(): Unit = {
    val tiles = findTilesPositions().head
    val ids = tiles map {case (pos, (id, _)) => pos -> id}
    val corners = Seq(0, sideSize - 1) replicateA 2

    println(s"Part 1: ${corners map{case x::y::_ => ids(x, y)} reduce{_ * _}}")
  }

  def part2(): Unit = {
    val tiles = findTilesPositions().head
    val (_, (_, tile)) = tiles.head
    val tileSize = tile.size - 2
    val rowSize = sideSize * tileSize
    val picture = fill(rowSize , rowSize)('?')

    for {
      x::y::_   <- 0 until sideSize to List replicateA 2
      (_, tile)  = tiles(x, y)
      reduced    = tile map {_ to Array slice (1, tile.size - 1)} to Array slice (1, tile.size - 1)
      (row, r)  <- reduced.zipWithIndex
      (col, c)  <- row.zipWithIndex
      nRow = x * tileSize
      nCol = y * tileSize
    } picture(nRow + r)(nCol + c) = col

    val waters = picture.flatten.count{_ == '#'}
    val slices = for {
      rotation <- picture.rotations
      flip     <- rotation.flips
      rows      = flip sliding monsterRows
      cols      = rows map{_ map{_ sliding monsterCols to Iterable} to Iterable}
      arr       = cols map{_.zipItself}
    } yield arr

    val monsterAppearances = for {
      pictureSlices <- slices
      monstersCount = pictureSlices.map{_ count matchMonster}.sum if monstersCount > 0
    } yield monstersCount

    println(s"Part 2: ${waters - monsterAppearances.max * monsterSize}")
  }

  part1()
  part2()
}